#!/bin/bash

# By Omar Davila omardavila64@gmail.com

CONFIGDIR=hosts
LOGFILE=no-ip.log

# Crear directorios sino existen
if [ ! -e $CONFIGDIR ]; then
	mkdir $CONFIGDIR
fi

# Crear archivos sino existen
if [ ! -e $LOGFILE ]; then
        touch $LOGFILE
fi

CURRENTIP=$(wget http://ipinfo.io/ip -qO -)

# Leyendo directorio de hosts
for file in $CONFIGDIR/*.no-ip.cfg
do
	source $file

	# Extrayendo nombre base del archivo
	TMP_FILENAME1=$(basename $file)
	TMP_FILENAME2=${TMP_FILENAME1%.*}
	CFGNAME=${TMP_FILENAME2%.*}

	LOGDATE=$(date +"%Y-%m-%d %H:%M:%S")


	RESPONSE=$(curl -Gs "https://$USERNAME:$PASSWORD@dynupdate.no-ip.com/nic/update?hostname=$HOST&myip=$CURRENTIP")
	RESPONSE_ST=$(echo $RESPONSE | awk '{ print $1 }')

	if [ "$RESPONSE_ST" = "good" ]; then
		LOG="[$LOGDATE] (good) Host \"$HOST\" successfully updated to $CURRENTIP."

	elif [ "$RESPONSE_ST" = "nochg" ]; then
		LOG="[$LOGDATE] (nochg) IP address is current \"$HOST\", no update required."

	elif [ "$RESPONSE_ST" = "nohost" ]; then
		LOG="[$LOGDATE] (nohost) Invalid hostname \"$HOST\" in file: $TMP_FILENAME1."

	elif [ "$RESPONSE_ST" = "badauth" ]; then
		LOG="[$LOGDATE] (badauth) Invalid username or password combination in file: $TMP_FILENAME1."

	elif [ "$RESPONSE_ST" = "abuse" ]; then
		LOG="[$LOGDATE] (abuse) Username: $USERNAME in file: $TMP_FILENAME1, is bloqued due to abuse."

	elif [ "$RESPONSE_ST" = "911" ]; then
		LOG="[$LOGDATE] (911) A fatal error on our side such as a database outage. Retry the update in no sooner than 30 minutes."

	else
		LOG="[$LOGDATE] (error) $RESPONSE"
	fi

	echo $LOG >> $LOGFILE


done

exit 0
