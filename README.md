# Cron No-IP Updater


Scrip bash para actualizar IP de multiples hosts dinamicos en [No IP](https://www.noip.com)

__Requisitos__

1.  Tener instalado curl

__Uso__

1.  Crear los respectivos archivos de configuracion en el directorios hosts con la siguiente estructura de nombres ***testy.no-ip.cfg***, el contenido del archivo debe ser el siguiente:
* USERNAME=my_username
* PASSWORD=my_password
* HOSTNAME=[test.ddns.net](#)
2.  Otorgar permisos de ejecuacion al archivo [***no-ip.sh***](#): (`chmod +x`)
3.  Configurar el cron ****/15 * * * * /home/usuario/scripts/no-ip.sh***

***By Omar Dávila***